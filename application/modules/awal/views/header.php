<!DOCTYPE html>
<html>

<head>
	<title>BLOG CMS</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/bootstrap.css">

	<style type="text/css">
      .navbar{
  				background-color:  White;
       }
			 .li{
				 background-color:white;
			 }
    </style>
</head>

<body style="background-color:#BBDEFB">
<header>
<nav class="navbar navbar-default navbar-fixed-top">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="<?php echo base_url().'index.php/awal/index'?>">Blog</a>
    </div>
    <ul class="nav navbar-nav">
      <li><a href="<?php echo base_url().'index.php/awal/index'?>">Home</a></li>
    </ul>
		<ul class="nav navbar-nav navbar-right">
		<li><button type="button" class="btn btn-link btn-md" data-toggle="modal" data-target="#myModal">Login</button></li>
			</ul>
  </div>
</nav>
</header>
	<!-- Modal -->
	<div id="myModal" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<!-- konten modal-->
			<div class="modal-content">
				<!-- heading modal -->
				<div class="modal-header">
					<h3 class="modal-title">Silahkan Login</h3>
				</div>
				<!-- body modal -->
				<form method="post" action="<?php echo base_url().'index.php/awal/masuk'?>">
				<div class="modal-body">
				<input type="text" name="username" class="form-control" placeholder="Username" required/><br/>
				<input type="password" name="password" class="form-control" placeholder="Password" required/><br/>
				<button type="submit" class="btn btn-primary" name="submit">Log In</button>
				
				</div>
				</form>
				<!-- footer modal -->
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
				</div>
			</div>
		</div>
	</div>