<?php
class Awal extends CI_Controller{
    
    function __construct(){
		parent::__construct();
        $this->load->model('m_blog');
    }
    
    function index(){	
		$x['data']=$this->m_blog->get_all_berita();
		$this->load->view('header');
    $this->load->view('v_post_list_b',$x);
    $this->load->view('footer');

		
    }

    function view(){
      $kode=$this->uri->segment(3);
      $x['data']=$this->m_blog->get_berita_by_kode($kode);
      $this->load->view('header');
      $this->load->view('v_post_view',$x);
      $this->load->view('footer');
    }

    function masuk()
    {
    if(isset($_POST['submit'])){
      $username = $this->input->post('username');
      $password = $this->input->post('password');
      $berhasil = $this->m_blog->login($username,$password);
      if($berhasil == 1){
				$this->session->set_userdata(array('status_login'=>'sukses'));
			
				redirect('admin');
      }else{
        redirect('awal/index');
      }

    }else{
			redirect('awal/index');
    }
	}


    
}