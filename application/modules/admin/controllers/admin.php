<?php
class Admin extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model('m_blog');
		$this->load->library('upload');
	}
	
	function index(){	
		$x['data']=$this->m_blog->get_all_berita();
		$this->load->view('header');
		$this->load->view('v_post_list',$x);
		$this->load->view('footer');
		//$this->load->view('v_post_news');
	}

	function simpan_post(){
		$config['upload_path'] = './assets/images/'; //path folder
	    $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
	    $config['encrypt_name'] = TRUE; //nama yang terupload nantinya

	    $this->upload->initialize($config);
	    if(!empty($_FILES['filefoto']['name'])){
	        if ($this->upload->do_upload('filefoto')){
	        	$gbr = $this->upload->data();
	            //Compress Image
	            $config['image_library']='gd2';
	            $config['source_image']='./assets/images/'.$gbr['file_name'];
	            $config['create_thumb']= FALSE;
	            $config['maintain_ratio']= FALSE;
	            $config['quality']= '100%';
	            $config['width']= 310;
	            $config['height']= 120;
	            $config['new_image']= './assets/images/'.$gbr['file_name'];
	            $this->load->library('image_lib', $config);
	            $this->image_lib->resize();

	            $gambar=$gbr['file_name'];
                $jdl=$this->input->post('judul');
                $berita=$this->input->post('berita');

				$this->m_blog->simpan_berita($jdl,$berita,$gambar);
				redirect('admin/index');
		}else{
			redirect('admin');
	    }
	                 
	    }else{
			redirect('admin');
		}
				
	}

	function tambah(){
		$this->load->view('header');
		$this->load->view('v_post_news');
		$this->load->view('footer');
		//$x['data']=$this->m_blog->get_all_berita();
		//$this->load->view('v_post_list',$x);
	}

	function view(){
		$kode=$this->uri->segment(3);
		$x['data']=$this->m_blog->get_berita_by_kode($kode);
		$this->load->view('header');
		$this->load->view('v_post_view',$x);
		$this->load->view('footer');
	}

	function deleteBer($berita_id){ //fungsi delete
    $this->load->model('m_blog'); //load model
    $this->m_blog->deleteB($berita_id); //ngacir ke fungsi delTransaksi
    redirect('admin/index'); //redirect
 
	}

	function keluar(){
		$this->session->sess_destroy();
			redirect('awal/index');
		}



}